#!/usr/bin/env bash

set +xe

CI_REGISTRY=registry.gitlab.com

docker logout

docker login ${CI_REGISTRY}

docker build --build-arg PDCI_COMMIT_SHA=${CI_COMMIT_SHA} --build-arg PDCI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME} --build-arg PDCI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA} --build-arg PDCI_COMMIT_TAG=${CI_COMMIT_TAG} --no-cache  --pull --target sql -t "${CI_REGISTRY}/sisicmbio/${PDCI_TEMPLATE_NAME_PROJETO}/image_file_sql:develop" .

docker push  "${CI_REGISTRY}/sisicmbio/${PDCI_TEMPLATE_NAME_PROJETO}/image_file_sql:develop"

docker build --build-arg PDCI_COMMIT_SHA=${CI_COMMIT_SHA} --build-arg PDCI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME} --build-arg PDCI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA} --build-arg PDCI_COMMIT_TAG=${CI_COMMIT_TAG} --no-cache  --pull --target develop -t "${CI_REGISTRY}/sisicmbio/${PDCI_TEMPLATE_NAME_PROJETO}:develop" .

docker push  "${CI_REGISTRY}/sisicmbio/${PDCI_TEMPLATE_NAME_PROJETO}:develop"

