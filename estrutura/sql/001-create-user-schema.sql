DO
$do$
BEGIN
 IF NOT EXISTS (
     SELECT                       -- SELECT list can stay empty for this
     FROM   pg_catalog.pg_roles
     WHERE  rolname = 'usr_${PDCI_TEMPLATE_NAME_SISTEMA}') THEN

   create user usr_${PDCI_TEMPLATE_NAME_SISTEMA} with password 'usr_${PDCI_TEMPLATE_NAME_SISTEMA}';
 END IF;

--  IF NOT  EXISTS (
--      SELECT -- SELECT list can stay empty for this
--      FROM information_schema.tables  WHERE table_schema='${PDCI_TEMPLATE_NAME_SISTEMA}'
--  )
--  THEN
--   CREATE SCHEMA IF NOT EXISTS ${PDCI_TEMPLATE_NAME_SISTEMA};
--
--  END IF;
 CREATE SCHEMA IF NOT EXISTS ${PDCI_TEMPLATE_NAME_SISTEMA};

 grant all on all tables in schema ${PDCI_TEMPLATE_NAME_SISTEMA} to usr_${PDCI_TEMPLATE_NAME_SISTEMA};
 grant usage on all sequences in schema ${PDCI_TEMPLATE_NAME_SISTEMA} to usr_${PDCI_TEMPLATE_NAME_SISTEMA};
 grant execute on all functions in schema ${PDCI_TEMPLATE_NAME_SISTEMA} to usr_${PDCI_TEMPLATE_NAME_SISTEMA};

 grant all on all tables in schema ${PDCI_TEMPLATE_NAME_SISTEMA} to usr_jenkins;
 grant usage on all sequences in schema ${PDCI_TEMPLATE_NAME_SISTEMA} to usr_jenkins;
 grant execute on all functions in schema ${PDCI_TEMPLATE_NAME_SISTEMA} to usr_jenkins;

END;
$do$;