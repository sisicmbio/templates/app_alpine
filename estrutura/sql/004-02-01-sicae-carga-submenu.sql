DO
$$
    DECLARE
        v_no_funcionalidade text;
        v_sq_sistema integer;
        v_sq_perfil integer;
        v_return_excluir_schema_sistema text;
        v_sq_funcionalidade integer;
        v_json_return json;
        v_no_perfil_analista text;
        v_no_perfil_pesquisador text;
        v_no_perfil_administrador text;
        v_no_perfil_gestor text;
        v_sg_sistema text;
    BEGIN
        v_no_perfil_analista:= 'Analista';
        v_no_perfil_pesquisador:= 'Pesquisador(ra)';
        v_no_perfil_administrador:= 'Administrador';
        v_no_perfil_gestor:= 'Gestor';
        v_sg_sistema:= '${PDCI_TEMPLATE_UPPER_NAME_SISTEMA}';

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade_sub_menu(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Avaliador(ra)'::text,
        'Projetos'::text,
        'Visualização e avaliação do projeto'::text,
        'EvaluationShow');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_SUBMENU%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade_sub_menu(
        v_sg_sistema,
        v_no_perfil_analista,
        'Avaliador(ra)'::text,
        'Projetos'::text,
        'Visualização e avaliacao do projeto'::text,
        'EvaluationShow');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_SUBMENU%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade_sub_menu(
        v_sg_sistema,
        v_no_perfil_analista,
        'Avaliador(ra)'::text,
        'Projetos'::text,
        'Visualizacao de avaliacao por Id'::text,
        'EvaluationShow');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_SUBMENU%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    END
$$;
