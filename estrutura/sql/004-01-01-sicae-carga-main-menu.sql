DO
$$
    DECLARE
        v_no_funcionalidade text;
        v_sq_sistema integer;
        v_sq_perfil integer;
        v_return_excluir_schema_sistema text;
        v_sq_funcionalidade integer;
        v_json_return json;
        v_no_perfil_analista text;
        v_no_perfil_pesquisador text;
        v_no_perfil_administrador text;
        v_no_perfil_gestor text;
        v_sg_sistema text;
    BEGIN
    v_no_perfil_analista:= 'Analista';
    v_no_perfil_pesquisador:= 'Pesquisador(ra)';
    v_no_perfil_administrador:= 'Administrador';
    v_no_perfil_gestor:= 'Gestor';
    v_sg_sistema:= '${PDCI_TEMPLATE_UPPER_NAME_SISTEMA}';


        v_sq_sistema:=sicae.pdci_add_sistema(
            '${PDCI_TEMPLATE_UPPER_NAME_SISTEMA}',
            v_sg_sistema,
            'Projeto ${PDCI_TEMPLATE_NAME_SISTEMA}',
            '69974349168',
            true);

        RAISE INFO '-- %SQ_SISTEMA=%', E'\n', v_sq_sistema;

          v_sq_perfil:=sicae.pdci_add_perfil(
              v_sg_sistema ,
              v_no_perfil_analista,
              false ,
              3);

          v_sq_perfil:=sicae.pdci_add_perfil(
              v_sg_sistema ,
              v_no_perfil_gestor,
              false ,
              2);


         RAISE INFO '-- %SQ_PERFIL=%', E'\n', v_sq_perfil;

         v_sq_perfil:=sicae.pdci_add_perfil(
              v_sg_sistema ,
              v_no_perfil_administrador,
              false ,
              3);


         RAISE INFO '-- %SQ_PERFIL=%', E'\n', v_sq_perfil;

        v_sq_perfil:=sicae.pdci_add_perfil(
                v_sg_sistema ,
                v_no_perfil_pesquisador,
                false ,
                3);

        RAISE INFO '-- %SQ_PERFIL=%', E'\n', v_sq_perfil;



   v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Início',
        'Página Home',
        'Home');
   RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;


   v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Início',
        'Página Home',
        'Home');
   RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

   v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_pesquisador,
        'Início',
        'Página Home',
        'Home');
   RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

   v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_analista,
        'Início',
        'Página Home',
        'Home');
   RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END INICIO --------------------------------

    -- ------------------------------------ NOVO PROJETO ------------------------------------

    -- NOVO PROJETO	-	PESQUISADOR(RA)	            /projetos/novo	                CRIACAO DE UM NOVO PROJETO
    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Novo Projeto',
        'Criação de um novo projeto',
        'ProjectCreate');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Novo Projeto',
        'Criação de um novo projeto',
        'ProjectCreate');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_pesquisador,
        'Novo Projeto',
        'Criação de um novo projeto',
        'ProjectCreate');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END NOVO PROJETO ------------------------------------

    -- ------------------------------------ MEUS PROJETOS ------------------------------------

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
            v_sg_sistema,
            v_no_perfil_gestor,
            'Meus projetos',
            'Lista de Projetos Cadastrados pelo pesquisador',
            'UserProjects');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;
    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
            v_sg_sistema,
            v_no_perfil_administrador,
            'Meus projetos',
            'Lista de Projetos Cadastrados pelo pesquisador',
            'UserProjects');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
            v_sg_sistema,
            v_no_perfil_pesquisador,
            'Meus projetos',
            'Lista de Projetos Cadastrados pelo pesquisador',
            'UserProjects');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END MEUS PROJETOS ------------------------------------

    -- ------------------------------------ ADMIN-PROJETOS ---------------------------------------

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Admin',
        'Lista de projetos cadastrados disponíveis',
        'AdminProjetos');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;
    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Admin',
        'Lista de projetos cadastrados disponíveis',
        'AdminProjetos');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END ADMIN-PROJETOS ------------------------------------

    -- ------------------------------------ ADMIN-USUARIOS ----------------------------------------

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Admin',
        'Lista de usuarios disponíveis',
        'AdminUsers');

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Admin',
        'Lista de usuarios disponíveis',
        'AdminUsers');

    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END ADMIN-USUARIOS ------------------------------------

    -- ------------------------------------ ADMIN-AVALIADORES -------------------------------------

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Admin',
        'Lista de avaliadores disponíveis',
        'AdminEvaluators');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Admin',
        'Lista de avaliadores disponíveis',
        'AdminEvaluators');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END ADMIN-AVALIADORES ---------------------------------

    -- ------------------------------------ AVALIADORES-PROJETOS ----------------------------------

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Avaliador(ra)',
        'Lista de avaliacoes',
        'EvaluatorProjects');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Avaliador(ra)',
        'Lista de avaliacoes',
        'EvaluatorProjects');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_analista,
        'Avaliador(ra)',
        'Lista de avaliacoes',
        'EvaluatorProjects');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END AVALIADORES-PROJETOS ------------------------------


    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Início',
        'Visualização do perfil do usuário logado',
        'Profile');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Início',
        'Visualização do perfil do usuário logado',
        'Profile');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_pesquisador,
        'Início',
        'Visualização do perfil do usuário logado',
        'Profile');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_analista,
        'Início',
        'Visualização do perfil do usuário logado',
        'Profile');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END PERFIL ------------------------------------

    -- ------------------------------------ EDITAR PROJETO --------------------------------

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Novo Projeto',
        'Edição de um projeto ou continuação do rascunho.',
        'ProjectEdit');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;
    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Novo Projeto',
        'Edição de um projeto ou continuação do rascunho.',
        'ProjectEdit');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_pesquisador,
        'Novo Projeto',
        'Edição de um projeto ou continuação do rascunho.',
        'ProjectEdit');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END EDITAR PROJETO ----------------------------

    -- ------------------------------------ VISUALIZAR PROJETO ----------------------------

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Meus projetos',
        'Visualização do projeto específico.',
        'ProjectShow');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Meus projetos',
        'Visualização do projeto específico.',
        'ProjectShow');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_pesquisador,
        'Meus projetos',
        'Visualização do projeto específico.',
        'ProjectShow');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -----------------

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_gestor,
        'Avaliador(ra)',
        'Visualização do projeto específico.',
        'EvaluationShow');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_administrador,
        'Avaliador(ra)',
        'Visualização do projeto específico.',
        'EvaluationShow');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    v_sq_funcionalidade:=sicae.pdci_add_funcionalidade(
        v_sg_sistema,
        v_no_perfil_analista,
        'Avaliador(ra)',
        'Visualização do projeto específico.',
        'EvaluationShow');
    RAISE INFO '-- %SQ_FUNCIONALIDADE_%=%', E'\n', upper(v_no_funcionalidade),v_sq_funcionalidade;

    -- ------------------------------------ END VISUALIZAR PROJETO ------------------------

    v_json_return:= sicae.pdci_add_acesso_sicae(
            v_sg_sistema,
            v_no_perfil_pesquisador,
            '{"69974349168","06458687109", "24852929300"}',
            'COTEC');

    v_json_return:= sicae.pdci_add_acesso_sicae(
            v_sg_sistema,
            v_no_perfil_gestor,
            '{"69974349168","06458687109", "24852929300"}',
            'COTEC');

    v_json_return:= sicae.pdci_add_acesso_sicae(
            v_sg_sistema,
            v_no_perfil_administrador,
            '{"69974349168","06458687109", "24852929300"}',
            'COTEC');

    v_json_return:= sicae.pdci_add_acesso_sicae(
            v_sg_sistema,
            v_no_perfil_analista,
            '{"69974349168","06458687109", "24852929300"}',
            'COTEC');

    RAISE INFO  '%RESULTADO % %' , E'\n\n\n' ,jsonb_pretty(v_json_return::jsonb), E'\n\n\n';

    END
$$;
