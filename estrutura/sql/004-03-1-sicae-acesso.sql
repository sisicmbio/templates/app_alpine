﻿/**
  Este script esta comentado para ser utilizado como exemplo
 */
DO
$$
    DECLARE
        v_sg_sistema                text;
        v_sg_unidade_organizacional text;
        v_nu_cpf_acesso             text[];
        v_json_return               json;
    BEGIN
        v_sg_sistema := '${PDCI_TEMPLATE_UPPER_NAME_SISTEMA}';
        v_sg_unidade_organizacional := 'CGTI';
        v_nu_cpf_acesso := '{"69974349168","01863706100","72345098104","04742462130","00325307148"}';

        v_json_return := sicae.pdci_add_acesso_sicae(
                v_sg_sistema,
                'Administrador',
                v_nu_cpf_acesso,
                v_sg_unidade_organizacional);

        RAISE INFO '%RESULTADO % %' , E'\n\n\n' ,jsonb_pretty(v_json_return::jsonb), E'\n\n\n';

    END
$$;



