# ${PDCI_TEMPLATE_NAME_SISTEMA}

dev.${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO} (Ambiente de Desenvolvimento)
tcti${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO}
hmg${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO}
${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO} (será criado futuramente)


## Visão 
Desenvolver um sistema web para atender a necessidades do [...] de acordo com as metodologias especificadas pelo Instituto Chico Mendes de Biodiversidade (ICMBio).

# Preparando ambiente de desenvolvimento.

## Pre-requisitos:

git (https://git-scm.com/)

vagrant (https://www.vagrantup.com/downloads.html)

virtualbox (https://www.virtualbox.org/)

IDE desenvolvedor Ex: IntelliJ IDEA (https://download.jetbrains.com/idea/ideaIC-2020.1.3.exe?_ga=2.57691747.858246651.1594307084-667136129.1594307084) 

Espaço livre de 150 Gigas

4 Gigas de Mémora Ran



### Realize o clone do projeto no seu computador local.

#### linha de comando

`git clone https://gitlab.com/sisicmbio/app_${PDCI_TEMPLATE_NAME_SISTEMA}.git`

#### Pelo IntelliJ IDEA

Abra o programa e siga os passos da seguinte tela:

`File => New => Projeto From Version Controle ...`


### Criação da Máquina virtual no virtual box.

O vagrant irá criar uma máquina virtual no virtual box que será utilizado como nosso docker host de desenvolvimento. Será instalado e configurado automaticamente todo 
o ambiente de desenvolvimento atraves do script de automatização do vagrant. O script de automatização é o arquivo Vagrantfile dentro do seu projeto. 

1) E necesserário que o vagrant e o virtual box já esteja instalado no seu sistema operacional. 
2) Para maquinas windows abra o prompt em modo administrador.
3) Dentro da pasta do seu projeto digite `vagrant up` e espere a finalização da instalação automatizada

### Sicronização da pasta local do seu projeto com o docker host de desenvolvimento. 

Agora todos os seus aquivos editados na sua máquina local serão sicronizados automaticamente com o docker host. 
Utilize o programa putty (https://www.putty.org/) para uma melhor experiencia. 
Realize o acesso via ssh através do putty com os seguintes parametros:

Host Name: 192.168.56.133
Port 22
Usuário: root
Senha: vagrant

todo o seu código fonte clonado esta na pasta com /app_${PDCI_TEMPLATE_NAME_SISTEMA}

cd /app_${PDCI_TEMPLATE_NAME_SISTEMA}

## Deploy

Deploy significa colocar em posição. Em outras palavras disponibilizar um sistema para uso, seja num ambiente de desenvolvimento, para testes, homologação ou em produção.

### Deploy do banco de dados com o ultimo dump de produção no ambiente de desenvolvimento conhecido internacionamente como DEV :


1) Subindo container com banco de dados no DEV

De permissão de execução no arquivo de deploy do DB:

     ```
     chmod +x deploy_dev__db_docker-compose.sh
     ```
     
Execute o script de deploy e siga as intruções.     
          ```
          ./deploy_dev__db_docker-compose.sh dev
          ```

utilize o comando para verificar a porta que vc pode utilizar para se contectar ao banco pelo windows. 

docker ps | grep dev_${PDCI_TEMPLATE_NAME_SISTEMA}_db_1



2) Subindo suite da aplicação no DEV

De permissão de execução no arquivo de deploy da app:

     ```
     chmod +x deploy_dev__docker-compose.sh
 
     ```
     
Execute o script de deploy e siga as intruções.     
        
          ```
          ./deploy_dev__docker-compose.sh dev
        
          ```     

Neste ponto vc deve ter os containers rodando em sua máquina.

Importante configurar o seu arquivo `/etc/hosts` com as seguintes linhas (substituindo pelo seu IP)

```
192.168.56.133	sicae.${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO}
192.168.56.133	dev.${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO}

```

Pode acessar o http://dev.${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO} para verificar se a aplicação com a última imagem em develop está rodando.

Pode acessar o http://sicae.${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO} para acessar o sicae no ambiente de desenvolvimento.

OBS: Todos os usuários internos e externos estão com a mesma senha: qaz123

          
#### Deploy em TCTI

Acessar o jenkins em jenkins.icmbio.gov.br

Acesse a aba do projeto ${PDCI_TEMPLATE_NAME_SISTEMA}

#### Deploy em HMG

#### Deploy em PRD

#### Abertura de chamados para a infra estrutura de Banco de Dados.

1) Abertura de chamado de banco de dados para o ambiente TCTI.

- 1.1) Criação do banco de dados para o projeto ${PDCI_TEMPLATE_NAME_SISTEMA} caso não exista:

   ~~~ sql 
     CREATE DATABASE db_tcti_${PDCI_TEMPLATE_NAME_SISTEMA}
     WITH OWNER = usr_jenkins
     ENCODING = 'UTF8'
     TABLESPACE = pg_default
     LC_COLLATE = 'en_US.UTF-8'
     LC_CTYPE = 'en_US.UTF-8'
     CONNECTION LIMIT = -1;
     COMMENT ON DATABASE db_tcti_${PDCI_TEMPLATE_NAME_SISTEMA}
     IS '${PDCI_TEMPLATE_NAME_SISTEMA}';
   ~~~

- 1.2) Configuração do pg_hba do banco de dados de TCTI:

  Adicionar a permissão para o usuário "usr_jenkins" possa ter acesso ao banco "db_tcti_${PDCI_TEMPLATE_NAME_SISTEMA}" através do ip 10.197.32.76, caso não exista;

- 1.3) Criar o usuário usr_${PDCI_TEMPLATE_NAME_SISTEMA} com a senha usr_${PDCI_TEMPLATE_NAME_SISTEMA};

- 1.4) Favor verificar se as configuração do arquivo pg_hba do ambiente de TCTI
  para o usuário "usr_${PDCI_TEMPLATE_NAME_SISTEMA}" no banco de dados "db_tcti_${PDCI_TEMPLATE_NAME_SISTEMA}"
  e fazer as alterações necessárias para conceder a seguinte permissão:

  * Ambiente: TCTI
  * Usuário: usr_${PDCI_TEMPLATE_NAME_SISTEMA}
  * Database: db_tcti_${PDCI_TEMPLATE_NAME_SISTEMA}
  * Seguintes IPs:
     * 10.10.4.216
     * 10.10.4.217
     * 10.10.4.218
     * 10.10.4.219

2) Abertura de chamado de banco de dados para o ambiente de Homologação:

- 2.1) Criação do banco de dados para o projeto ${PDCI_TEMPLATE_NAME_SISTEMA}:

          ~~~ sql 
          CREATE DATABASE db_${PDCI_TEMPLATE_NAME_SISTEMA}
          WITH OWNER = usr_jenkins
          ENCODING = 'UTF8'
          TABLESPACE = pg_default
          LC_COLLATE = 'en_US.UTF-8'
          LC_CTYPE = 'en_US.UTF-8'
          CONNECTION LIMIT = -1;
          COMMENT ON DATABASE db_${PDCI_TEMPLATE_NAME_SISTEMA}
          IS '${PDCI_TEMPLATE_NAME_SISTEMA}';
          ~~~

- 2.2) Configuração do pg_hba do banco de dados de Homologação:

            Adicionar a permissão para o usuário "usr_jenkins" possa ter acesso ao banco "db_${PDCI_TEMPLATE_NAME_SISTEMA}" através do ip 10.197.32.76

- 2.3) Criar o usuário usr_${PDCI_TEMPLATE_NAME_SISTEMA} com a senha usr_${PDCI_TEMPLATE_NAME_SISTEMA};

- 2.4) Favor verificar se as configuração do arquivo pg_hba do ambiente de Homologação
            para o usuário "usr_${PDCI_TEMPLATE_NAME_SISTEMA}" no banco de dados "db_${PDCI_TEMPLATE_NAME_SISTEMA}"
            e fazer as alterações necessárias para conceder a seguinte permissão:

* Ambiente: Homologação
* Usuário: usr_${PDCI_TEMPLATE_NAME_SISTEMA}
* Database: db_${PDCI_TEMPLATE_NAME_SISTEMA}
* Seguintes IPs:
  * 10.10.4.216
  * 10.10.4.217
  * 10.10.4.218
  * 10.10.4.219

3) Abertura de chamado de banco de dados para o ambiente de Treinamento em produção:

- 3.1) Criação do banco de dados para o projeto ${PDCI_TEMPLATE_NAME_SISTEMA}:

     ~~~ sql 

      CREATE DATABASE db_trn_${PDCI_TEMPLATE_NAME_SISTEMA}
      WITH OWNER = usr_jenkins
      ENCODING = 'UTF8'
      TABLESPACE = pg_default
      LC_COLLATE = 'en_US.UTF-8'
      LC_CTYPE = 'en_US.UTF-8'
      CONNECTION LIMIT = -1;
      COMMENT ON DATABASE db_trn_${PDCI_TEMPLATE_NAME_SISTEMA}
      IS '${PDCI_TEMPLATE_NAME_SISTEMA}';

     ~~~

- 3.2) Configuração do pg_hba do banco de dados de Treinamento:

     Adicionar a permissão para o usuário "usr_jenkins" possa ter acesso ao banco "db_trn_${PDCI_TEMPLATE_NAME_SISTEMA}" através do ip 10.197.32.76

- 3.3) Favor verificar se as configuração do arquivo pg_hba do ambiente de Treinamento em produção
     para o usuário "usr_${PDCI_TEMPLATE_NAME_SISTEMA}" no banco de dados "db_trn_${PDCI_TEMPLATE_NAME_SISTEMA}"
     e fazer as alterações necessárias para conceder a seguinte permissão:

   * Ambiente: Treinameto em produção
   * Usuário: usr_${PDCI_TEMPLATE_NAME_SISTEMA}
   * Database: db_trn_${PDCI_TEMPLATE_NAME_SISTEMA}
   * Seguintes IPs:
     * 10.10.4.216
     * 10.10.4.217
     * 10.10.4.218
     * 10.10.4.219

4) Abertura de chamado de banco de dados para o ambiente em Produção:

- 4.1) Criação do banco de dados para o projeto ${PDCI_TEMPLATE_NAME_SISTEMA}:

     ~~~ sql 
     
     CREATE DATABASE db_${PDCI_TEMPLATE_NAME_SISTEMA}
     WITH OWNER = usr_jenkins
     ENCODING = 'UTF8'
     TABLESPACE = pg_default
     LC_COLLATE = 'en_US.UTF-8'
     LC_CTYPE = 'en_US.UTF-8'
     CONNECTION LIMIT = -1;
     COMMENT ON DATABASE db_${PDCI_TEMPLATE_NAME_SISTEMA}
     IS '${PDCI_TEMPLATE_NAME_SISTEMA}'; 
     
     ~~~

- 4.2) Configuração do pg_hba do banco de dados de Produção:

     Adicionar a permissão para o usuário "usr_jenkins" possa ter acesso ao banco "db_${PDCI_TEMPLATE_NAME_SISTEMA}" através do ip 10.197.32.76

- 4.3) Criar o usuário usr_${PDCI_TEMPLATE_NAME_SISTEMA} com uma senha aleatória e salvar esta referida senha no cofre do jenkins em
     https://jenkins.icmbio.gov.br/credentials/store/system/domain/_/credential/db_prd_usr_${PDCI_TEMPLATE_NAME_SISTEMA}/update;

- 4.4) Favor verificar se as configuração do arquivo pg_hba do ambiente de produção
     para o usuário "usr_${PDCI_TEMPLATE_NAME_SISTEMA}" no banco de dados "db_${PDCI_TEMPLATE_NAME_SISTEMA}"
     e fazer as alterações necessárias para conceder a seguinte permissão:

   * Ambiente: Produção
   * Usuário: usr_${PDCI_TEMPLATE_NAME_SISTEMA}
   * Database: db_${PDCI_TEMPLATE_NAME_SISTEMA}
   * Seguintes IPs:
      * 10.197.32.218 ( Hostname: svdf01218-docker-work-01 )
      * 10.197.32.219 ( Hostname: svdf01219-docker-work-02 )
      * 10.197.32.220 ( Hostname: svdf01220-docker-work-03 )




5)Abertura de chamado para aplicação em TCTI

- 5.1) Criação do domínio tcti${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO};

- 5.2) Criação do virtual host do domínio tcti${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO}
  Apontar para o ingress no cluster do MMA (10.10.4.217) utilizando o https do letsencrypt.

6)Abertura de chamado para aplicação em Homologação

- 6.1) Criação do domínio hmg${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO};

- 6.2) Criação do virtual host do domínio hmg${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO}
  Apontar para o ingress no cluster do MMA (10.10.4.217) utilizando o https do letsencrypt.

7)Abertura de chamado para aplicação em Treinamento

- 7.1) Criação do domínio trn${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO};

- 7.2) Criação do virtual host do domínio trn${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO}
  Apontar para o ingress no cluster do MMA (10.10.4.217) utilizando o https do letsencrypt.

8)Abertura de chamado para aplicação em Produção

- 8.1) Criação do domínio ${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO};

- 8.2) Criação do virtual host do domínio ${PDCI_TEMPLATE_NAME_SISTEMA}.${PDCI_TEMPLATE_DOMINIO}
  Apontar para o ingress no cluster do ICMBio (10.197.32.216,10.197.32.217) utilizando o https do letsencrypt.
