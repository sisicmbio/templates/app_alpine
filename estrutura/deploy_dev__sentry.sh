#!/usr/bin/env bash
rm -rf sentry-develop
rm -rf sentry-develop.tar.gz
wget https://gitlab.com/sisicmbio/pdci/sentry/-/archive/develop/sentry-develop.tar.gz
tar -zxf sentry-develop.tar.gz
rm -rf sentry-develop.tar.gz
cd sentry-develop
./deploy_dev__db_docker-compose.sh dev
./deploy_dev__docker-compose.sh dev
cd ..
rm -rf sentry-develop
