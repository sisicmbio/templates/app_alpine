#!/usr/bin/env bash

docker -H tcp://10.197.32.76:2375 logout
docker -H tcp://10.197.32.76:2375 login -u "$PDCI_CI_REGISTRY_USER" -p "$PDCI_CI_REGISTRY_PASSWORD" registry.gitlab.com

docker -H tcp://10.197.32.76:2375 pull registry.gitlab.com/pdci/postgis:latest
docker -H tcp://10.197.32.76:2375  tag registry.gitlab.com/pdci/postgis:latest registry.gitlab.com/sisicmbio/${PDCI_TEMPLATE_NAME_PROJETO}/postgis-dev:latest
docker -H tcp://10.197.32.76:2375 push registry.gitlab.com/sisicmbio/${PDCI_TEMPLATE_NAME_PROJETO}/postgis-dev:latest


docker -H tcp://10.197.32.76:2375 pull registry.gitlab.com/sisicmbio/app_soala/image_file_dump-dev:latest
docker -H tcp://10.197.32.76:2375  tag registry.gitlab.com/sisicmbio/app_soala/image_file_dump-dev:latest  registry.gitlab.com/sisicmbio/app_alpine/image_file_dump-dev:latest
docker -H tcp://10.197.32.76:2375 push registry.gitlab.com/sisicmbio/app_alpine/image_file_dump-dev:latest

docker -H tcp://10.197.32.76:2375 pull registry.gitlab.com/sisicmbio/app_soala/image_file_dump-dev:latest
docker -H tcp://10.197.32.76:2375  tag registry.gitlab.com/sisicmbio/app_soala/image_file_dump-dev:latest  registry.gitlab.com/sisicmbio/${PDCI_TEMPLATE_NAME_PROJETO}/image_file_dump-dev:latest

docker -H tcp://10.197.32.76:2375 push registry.gitlab.com/sisicmbio/${PDCI_TEMPLATE_NAME_PROJETO}/image_file_dump-dev:latest


