#!/usr/bin/env bash

#export PDCI_IPT_VIRTUAL_HOST_ESCAPED="${PDCI_IPT_VIRTUAL_HOST_PROTOCOL}\://${PDCI_IPT_VIRTUAL_HOST}"

#envsubst < /opt/monitora/config/users.xml.template > /opt/monitora/config/users.xml
#envsubst < /opt/monitora/config/ipt.properties.template > /opt/monitora/config/ipt.properties
rm -rf output
mkdir -p output
cp -r estrutura output/estrutura

export PDCI_TEMPLATE_NAME_PROJETO=${PDCI_TEMPLATE_NAME_PROJETO:-app_sistema}
export PDCI_TEMPLATE_NAME_SISTEMA=${PDCI_TEMPLATE_NAME_SISTEMA:-sistema}

#export $(sed 's/\x0D$//'  ./.env-app  | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | xargs)

export PDCI_TEMPLATE_UPPER_NAME_SISTEMA=${PDCI_TEMPLATE_NAME_SISTEMA^^}
#export PDCI_APPLICATION_ENV='$''{PDCI_APPLICATION_ENV}'
#export PDCI_GIT_BRANCH='$''{PDCI_GIT_BRANCH}'


## Lista das variaveis que devem ser mantidas nos templates
declare -a arr_var_names=("PDCI_DB_HOST"
"PDCI_DB_PORT"
"PDCI_DB_NAME"
"PDCI_DB_USER"
"PDCI_DB_PASSWORD"
"PDCI_DB_SCHEMA"
"PDCI_SICAE_VIRTUAL_PROTO"
"PDCI_SICAE_VIRTUAL_HOST"
"PDCI_MAIL_DRIVER"
"PDCI_MAIL_HOST"
"PDCI_MAIL_PORT"
"PDCI_MAIL_USERNAME"
"PDCI_MAIL_PASSWORD"
"PDCI_MAIL_ENCRYPTION"
"PDCI_MAIL_FROM_ADDRESS"
"PDCI_MAIL_FROM_NAME"
"v_envsubst_add"
"VIRTUAL_PORT"
"VIRTUAL_PROTO"
"v_i"
"v_envsubst"
"v_pdci"
"v_p"
"PDCI_PATH_DOCUMENT_ROOT"
"PDCI_SICAE_VIRTUAL_HOST"
"PDCI_PROJECT_NAME"
"_random"
"CST_PATH"
"PEM_PATH"
"KEY_PATH"
"PDCI_PATH_APP_CONF"
"PDCI_PATH_VIRTUAL_HOST_CONF"
"VIRTUAL_HOST"
"RANDOM"
"PDCI_FILE_ENV_DOCKER"
"PDCI_CI_REGISTRY_USER"
"PDCI_CI_REGISTRY_PASSWORD"
"PDCI_APPLICATION_ENV"
"PDCI_GIT_BRANCH"
"CI_COMMIT_SHA"
"CI_COMMIT_REF_NAME"
"CI_COMMIT_SHORT_SHA"
"CI_COMMIT_TAG"
"CI_COMMIT_REF_SLUG"
"CI_REGISTRY_IMAGE"
"CI_REGISTRY_USER"
"CI_REGISTRY_PASSWORD"
"CI_REGISTRY"
"PDCI_COMMIT_REF_NAME"
"PDCI_COMMIT_SHA"
"PDCI_COMMIT_SHORT_SHA"
"PDCI_COMMIT_TAG"
"APP_HOME")

## now loop through the above array
for i in "${arr_var_names[@]}"
do
  export $(echo "$i='$''{$i}'" | xargs)
done



for i in `find ./estrutura/ -type f`
do
    dir_destino=$(echo $i | sed -e "s|./estrutura/|./output/estrutura/|g" )
    echo "envsubst '${PDCI_TEMPLATE_UPPER_NAME_SISTEMA},${PDCI_TEMPLATE_NAME_PROJETO},${PDCI_TEMPLATE_NAME_SISTEMA},${PDCI_TEMPLATE_DB_NAME},${PDCI_TEMPLATE_DOMINIO}' < $i > $dir_destino"
    envsubst '${PDCI_TEMPLATE_UPPER_NAME_SISTEMA},${PDCI_TEMPLATE_NAME_PROJETO},${PDCI_TEMPLATE_NAME_SISTEMA},${PDCI_TEMPLATE_DB_NAME},${PDCI_TEMPLATE_DOMINIO}' < $i > $dir_destino
done

#Alterar o nome dos arquivos.
for i in `find ./output/estrutura/ -name PDCI_TEMPLATE_NAME_SISTEMA* -type f`
do
    file_destino=$(echo $i | sed -e "s|PDCI_TEMPLATE_NAME_SISTEMA|${PDCI_TEMPLATE_NAME_SISTEMA}|g" )
    echo "mv $i  $file_destino"
    mv $i  $file_destino

done

for i in `find ./output/estrutura/ -name PDCI_TEMPLATE_NAME_PROJETO* -type f`
do
    file_destino=$(echo $i | sed -e "s|PDCI_TEMPLATE_NAME_PROJETO|${PDCI_TEMPLATE_NAME_PROJETO}|g" )
    echo "mv $i  $file_destino"
    mv $i  $file_destino

done
